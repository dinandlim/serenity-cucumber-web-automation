# How to Run Serenity Cucumber Web Automation



## Getting started

Before running web automation testing in Katalon Studio, we need do some installation process first

You can follow the step bellow

## Installation

- Please Install [Intellij IDEA CE](https://www.jetbrains.com/idea/download/?section=windows)
- Please Install [Java](https://www.oracle.com/java/technologies/javase/javase8-archive-downloads.html)
- Please Install [Maven](https://maven.apache.org/download.cgi) 


## Run Web Automation

1. Open project in Intellij IDEA
2. Click Maven in the top right side of Intellij IDEA
3. Open Lifecycle - Click Clean - Click verify

## Note
Please contact me if you meet a problem
